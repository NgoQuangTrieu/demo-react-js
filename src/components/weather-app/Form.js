import React, {Component} from 'react';


class Form extends Component {
    render () {
        return (
            <form onSubmit = {this.props.loadWeather}>
                <div className="form-group">
                <label htmlFor="city">City address</label>
                <input type="text" className="form-control" name="city" id="city" aria-describedby="cityHelp" placeholder="Enter city" />
                </div>
                <div className="form-group">
                <label htmlFor="country">Country</label>
                <input type="text" className="form-control" name="country" id="country" placeholder="Enter country" />
                </div>
                <button type="submit" className="btn btn-primary">Get Weather</button>
            </form>
        );
    }
}

export default Form;